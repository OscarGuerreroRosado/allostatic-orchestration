# Drive competition underlies effective allostatic orchestration

Living systems ensure their fitness by self-regulating. The optimal matching of their behavior to
the opportunities and demands of the ever-changing natural environment is crucial for satisfying
physiological and cognitive needs. Although homeostasis has explained how organisms maintain
their internal states within a desirable range, the problem of orchestrating different homeostatic
systems has not been fully explained yet. In the present paper, we argue that attractor dynamics
emerge from the competitive relation of opposing drives, resulting in the effective regulation of
adaptive behaviors. To test this hypothesis, we develop a biologically-grounded attractor model
that allows allostatic orchestration when embedded in a synthetic agent. Results show that
the resultant neural mass model allows the agent to reproduce the navigational patterns of a
rodent in an open field. Moreover, when exploring the robustness of our model in a dynamically
changing environment, the synthetic agent pursues the stability of the self, being its internal
states dependent on environmental opportunities to satisfy its needs. Finally, we elaborate on
the benefits of resetting the model’s dynamics after drive-completion behaviors. Altogether, our
studies suggest that the neural mass allostatic model adequately reproduces self-regulatory
dynamics while overcoming the limitations of previous models.

## Repository

This repository includes the source code for the publication [Drive competition underlies effective allostatic orchestration](https://doi.org/10.3389/frobt.2022.1052998).
- [ ] [attractor.py](https://gitlab.com/OscarGuerreroRosado/allostatic-orchestration/-/blob/main/attractor.py). It is the attractor model imported in the following simulation scripts.
- [ ] [allostractor_StaticEnv.py](https://gitlab.com/OscarGuerreroRosado/allostatic-orchestration/-/blob/main/allostractor_StaticEnv.py). Simulation script for study 1: Open field test.
- [ ] [allostractor_DynamicEnv.py](https://gitlab.com/OscarGuerreroRosado/allostatic-orchestration/-/blob/main/allostractor_DynamicEnv.py). Simulation script for study 2: Dynamic environment.
- [ ] [allostractor_DynamicEnv_Inh.py](https://gitlab.com/OscarGuerreroRosado/allostatic-orchestration/-/blob/main/allostractor_DynamicEnv_Inh.py). Simulation script for study 3: Criticality-driven decision-reset.
- [ ] [q-var.py](https://gitlab.com/OscarGuerreroRosado/allostatic-orchestration/-/blob/main/q-var.py) script used to perform a parameter search in the attractor model.

In the [analisys/](https://gitlab.com/OscarGuerreroRosado/allostatic-orchestration/-/tree/main/analysis) folder, you can find the Jupyter notebooks used to analyze the data and report the results in our publication.

Additionally, the folder [Sanchez Fibla et al. (2010) replication/](https://gitlab.com/OscarGuerreroRosado/allostatic-orchestration/-/tree/main/Sanchez%20Fibla%20et%20al.%20(2010)%20replication) includes source code to replicate what, to our knowledge, is the [first computational model of allostatic orchestration](https://www.worldscientific.com/doi/abs/10.1142/S0219525910002621).


## Getting started

To run the experiments you only need to update the csv_namefile variable found in each simulation script. Define here the location where you would like to store the generated data. Be aware that analysis scripts also need to be updated with the location where you are storing the data.

Moreover, the bool variables plotting and Group_simulations in the simulation scripts will allow you to turn on/off the real-time visualizations and/or perform either single or several experiments.