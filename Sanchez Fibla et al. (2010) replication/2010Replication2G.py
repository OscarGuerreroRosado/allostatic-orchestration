#!/usr/bin/env python3
import matplotlib.pyplot as plt
import numpy as np
import random
import math
import csv


plots = False
Group_simulations=True
episodes = 35000
num_simulations=50

simulation = 0


arena_size = 200
arena_limit = 6

arenaX = [0,arena_size]
arenaY = [0,arena_size]
x_min=0
y_min=0
x_max=200
y_max=200


thetaList = []
theta = 90
counter = 0
lenght_counter = 0


nav_noise = 5
wheel_w = 15
weighting_factor_arousal=0.8 #0.8
weighting_factor_security=0.8 #0.8
th_arousal = 0.03

dv_low_arousal = 0.066
dv_high_arousal = 1
p_max_arousal = 0.003
p_min_arousal = 0.9

th_security = 0.05
dv_low_security = 0.166
dv_high_security = 1
p_max_security = 0.15
p_min_security = 0.01

dV_arousal = dv_high_arousal
dV_security = dv_high_security

security_intensity_list=[]
arousal_intensity_list=[]
aVarousal_list = []
dVarousal_list = []
aVsecurity_list = []
dVsecurity_list = []

plt.ion()
plt.style.use('seaborn')
fig1, ax1 = plt.subplots(1, 2,figsize=(8,4))
fig2, ax2 = plt.subplots(2, 1,figsize=(15,5))
fig2.tight_layout(pad=2.0)

class Allostasis_2010():
    def kde_quartic(self,d,h):
        dn=d/h
        P=(15/16)*(1-dn**2)**2
        return P

    def __init__(self):
        ## Initial position
        self.robot_x = [random.randint(10,190)]
        self.robot_y = [random.randint(10,190)]
        self.create_arousal_gradient()
        self.create_security_gradient()
        self.build_gradients()


    def create_arousal_gradient(self):
        #POINT DATASET
        x= [x_max / 2]
        y= [y_max / 2]
        #DEFINE GRID SIZE AND RADIUS(h)
        grid_size=1
        h=100
        #CONSTRUCT GRID
        x_grid=np.arange(x_min,x_max,grid_size)
        y_grid=np.arange(y_min,y_max,grid_size)
        self.x_mesh_arousal,self.y_mesh_arousal=np.meshgrid(x_grid,y_grid)
        #GRID CENTER POINT
        xc=self.x_mesh_arousal+(grid_size/2)
        yc=self.y_mesh_arousal+(grid_size/2)
        #PROCESSING
        for j in range(len(xc)):
            intensity_row=[]
            for k in range(len(xc[0])):
                kde_value_list=[]
                for i in range(len(x)):
                    #CALCULATE DISTANCE
                    d=math.sqrt((xc[j][k]-x[i])**2+(yc[j][k]-y[i])**2) 
                    if d<=h:
                        p=self.kde_quartic(d,h)
                    else:
                        p=0
                    kde_value_list.append(p)
                #SUM ALL INTENSITY VALUE
                p_total=sum(kde_value_list)
                intensity_row.append(p_total)
            arousal_intensity_list.append(intensity_row)


    def create_security_gradient(self):
        #POINTS DATASET
        x=[10]
        y=[arena_size-10]
        h=80

        #CONSTRUCT GRID
        grid_size=1
        x_grid=np.arange(x_min,x_max,grid_size)
        y_grid=np.arange(y_min,y_max,grid_size)
        self.x_mesh_security,self.y_mesh_security=np.meshgrid(x_grid,y_grid)
        #GRID CENTER POINT
        xc=self.x_mesh_security+(grid_size/2)
        yc=self.y_mesh_security+(grid_size/2)
        #PROCESSING
        for j in range(len(xc)):
            intensity_row=[]
            for k in range(len(xc[0])):
                kde_value_list=[]
                for i in range(len(x)):
                    #CALCULATE DISTANCE
                    d=math.sqrt((xc[j][k]-x[i])**2+(yc[j][k]-y[i])**2) 
                    if d<=h:
                        p=self.kde_quartic(d,h)
                    else:
                        p=0
                    kde_value_list.append(p)
                #SUM ALL INTENSITY VALUE
                p_total=sum(kde_value_list)
                intensity_row.append(p_total)
            security_intensity_list.append(intensity_row)


    def build_gradients(self):
        arousal_minimum = 100
        arousal_maximum = 0
        security_minimum = 100
        security_maximum = 0
        v_cnt = 0
        h_cnt = 0

        #look for min and max values
        for i in range(len(security_intensity_list)):
            for j in range(len(security_intensity_list[i])):
                if arousal_intensity_list[i][j] < arousal_minimum:
                    arousal_minimum = arousal_intensity_list[i][j]
                if arousal_intensity_list[i][j] > arousal_maximum:
                    arousal_maximum = arousal_intensity_list[i][j]
                if security_intensity_list[i][j] < security_minimum:
                    security_minimum = security_intensity_list[i][j]
                if security_intensity_list[i][j] > security_maximum:
                    security_maximum = security_intensity_list[i][j]


        #Normalize gradient
        security_intensity=np.array(security_intensity_list)
        security_intensity=security_intensity/security_maximum
        self.security_intensity=security_intensity
        

        arousal_intensity=np.array(arousal_intensity_list)
        arousal_intensity=arousal_intensity/arousal_maximum
        self.arousal_intensity=arousal_intensity

    def plot_gradients(self):
        ax1[0].cla()
        ax1[0].grid(False)
        ax1[0].plot(self.robot_x,self.robot_y,'ro', markersize=3)
        ax1[0].set_title("Arousal")
        ax1[0].pcolormesh(self.x_mesh_arousal,self.y_mesh_arousal,self.arousal_intensity, cmap = plt.get_cmap('viridis'))

        ax1[1].cla()
        ax1[1].grid(False)
        ax1[1].set_title("Security")
        ax1[1].plot(self.robot_x[-1],self.robot_y[-1],'ro')
        ax1[1].pcolormesh(self.x_mesh_security,self.y_mesh_security,self.security_intensity, cmap = plt.get_cmap('viridis'))

        fig1.canvas.flush_events()


        ax2[0].cla()
        ax2[0].grid(False)
        ax2[0].set_title("State of Security")
        ax2[0].set_ylim(-0.1, 1.1)
        ax2[0].plot(aVsecurity_list, color='green', label='Security aV')
        ax2[0].plot(dVsecurity_list, color='black', label='Security dV')
        ax2[0].legend(loc="upper right")

        ax2[1].cla()
        ax2[1].grid(False)
        ax2[1].set_title("State of Arousal")
        ax2[1].set_ylim(-0.1, 1.1)
        ax2[1].plot(aVarousal_list, color='red', label='Arousal aV')
        ax2[1].plot(dVarousal_list, color='black', label='Arousal dV')
        ax2[1].legend(loc="upper right")

        fig2.canvas.flush_events()


    def conv(self, ang):
        x = np.cos(np.radians(ang)) 
        y = np.sin(np.radians(ang)) 
        return x , y

    

    def arousal_av(self):
        global dV_arousal, aVarousal_list, dVarousal_list
        self.q0_arousal, self.q1_arousal, self.q2_arousal, self.q3_arousal = 0,0,0,0

        for i in range(4):
            for j in range(3):
                self.q0_arousal += self.arousal_intensity[int(self.robot_y[-1]) + (j + 1), int(self.robot_x[-1]) - (i + 1)]
                self.q1_arousal += self.arousal_intensity[int(self.robot_y[-1]) + (j + 1), int(self.robot_x[-1]) + (i + 1)]
                self.q2_arousal += self.arousal_intensity[int(self.robot_y[-1]) - (j + 1), int(self.robot_x[-1]) - (i + 1)]
                self.q3_arousal += self.arousal_intensity[int(self.robot_y[-1]) - (j + 1), int(self.robot_x[-1]) + (i + 1)]

        self.q0_arousal /= 12
        self.q1_arousal /= 12
        self.q2_arousal /= 12
        self.q3_arousal /= 12
        
        
        self.aV_arousal = (self.q0_arousal + self.q1_arousal + self.q2_arousal + self.q3_arousal) / 4
        aVarousal_list.append(self.aV_arousal)
        dVarousal_list.append(dV_arousal)


    def security_av(self):
        global dV_security, aVsecurity_list, dVsecurity_list, Usecurity_list
        self.q0_security, self.q1_security, self.q2_security, self.q3_security = 0,0,0,0
        for i in range(4):
            for j in range(3):
                self.q0_security += self.security_intensity[int(self.robot_y[-1]) + (j + 1), int(self.robot_x[-1]) - (i + 1)]
                self.q1_security += self.security_intensity[int(self.robot_y[-1]) + (j + 1), int(self.robot_x[-1]) + (i + 1)]
                self.q2_security += self.security_intensity[int(self.robot_y[-1]) - (j + 1), int(self.robot_x[-1]) - (i + 1)]
                self.q3_security += self.security_intensity[int(self.robot_y[-1]) - (j + 1), int(self.robot_x[-1]) + (i + 1)]

        self.q0_security /= 12
        self.q1_security /= 12
        self.q2_security /= 12
        self.q3_security /= 12

        
        self.aV_security = (self.q0_security + self.q1_security + self.q2_security + self.q3_security) / 4
        aVsecurity_list.append(self.aV_security)
        dVsecurity_list.append(dV_security)



    def adsign(self):
        global dV_arousal, dV_security
        self.adsign_arousal = np.sign(dV_arousal - self.aV_arousal)
        self.adsign_security = np.sign(dV_security - self.aV_security)


    def hsign(self):
        if theta <= 112 and theta > 77: #UP
            self.hsign_arousal = np.sign(self.q1_arousal - self.q0_arousal)
            self.hsign_security = np.sign(self.q1_security - self.q0_security)
        elif theta <= 157 and theta > 112: #UP-L
            self.hsign_arousal = np.sign(((self.q0_arousal + self.q1_arousal)/2) - ((self.q0_arousal + self.q2_arousal)/2))
            self.hsign_security = np.sign(((self.q0_security + self.q1_security)/2) - ((self.q0_security + self.q2_security)/2))
        elif theta <= 202 and theta > 157: #L
            self.hsign_arousal = np.sign(self.q0_arousal - self.q2_arousal)
            self.hsign_security = np.sign(self.q0_security - self.q2_security)
        elif theta <= 247 and theta > 202: #DOWN-L
            self.hsign_arousal = np.sign(((self.q2_arousal + self.q0_arousal)/2) - ((self.q2_arousal + self.q3_arousal)/2))
            self.hsign_security = np.sign(((self.q2_security + self.q0_security)/2) - ((self.q2_security + self.q3_security)/2))
        elif theta <= 292 and theta > 247: #DOWN
            self.hsign_arousal = np.sign(self.q2_arousal - self.q3_arousal)
            self.hsign_security = np.sign(self.q2_security - self.q3_security)
        elif theta <= 337 and theta > 292: #DOWN-R
            self.hsign_arousal = np.sign(((self.q3_arousal + self.q2_arousal)/2) - ((self.q3_arousal + self.q1_arousal)/2))
            self.hsign_security = np.sign(((self.q3_security + self.q2_security)/2) - ((self.q3_security + self.q1_security)/2))
        elif theta <= 22 and theta > 337: #R
            self.hsign_arousal = np.sign(self.q3_arousal - self.q1_arousal)
            self.hsign_security = np.sign(self.q3_security - self.q1_security)
        elif theta <= 77 and theta > 22: #UP-R
            self.hsign_arousal = np.sign(((self.q1_arousal + self.q3_arousal)/2) - ((self.q1_arousal + self.q0_arousal)/2))
            self.hsign_security = np.sign(((self.q1_security + self.q3_security)/2) - ((self.q1_security + self.q0_security)/2))


    def decision(self, probability):
        return random.random() < probability

    def prob_switcher(self):
        global dV_arousal, dV_security

        if abs(self.aV_arousal - dV_arousal) < th_arousal and dV_arousal <= dv_low_arousal:
            if self.decision(p_max_arousal) == True:
                dV_arousal = dv_high_arousal

        if abs(self.aV_arousal - dV_arousal) < th_arousal and dV_arousal >= dv_high_arousal:
            if self.decision(p_min_arousal) == True:
                dV_arousal = dv_low_arousal

        if abs(self.aV_security - dV_security) < th_security and dV_security <= dv_low_security:
            if self.decision(p_max_security) == True:
                dV_security = dv_high_security

        if abs(self.aV_security - dV_security) < th_security and dV_security <= dv_high_security:
            if self.decision(p_min_security) == True:
                dV_security = dv_low_security

    def total_force(self):
        global weighting_factor_arousal, weighting_factor_security

        self.total_force_arousal = weighting_factor_arousal * abs(self.aV_arousal - dV_arousal)
        self.total_force_security = weighting_factor_security * abs(self.aV_security - dV_security)

    def wheel_turning(self):
        self.wheel = -1 * ((self.hsign_arousal * self.adsign_arousal* self.total_force_arousal) + (self.hsign_security * self.adsign_security* self.total_force_security)) * (1/2)

    def random_navigation(self):
        global theta, counter, avoidance

        theta_extra = 5

        if theta > 360:
            div = math.trunc(theta/360) #num of rounds
            div *= 360                  #round in degrees
            theta = theta%div            #new theta
        if  theta < 0 and theta >= -360:
            theta +=360

        if(self.robot_x[-1]<arena_limit and self.robot_y[-1]<arena_limit): #Left-bottom
            theta = np.random.randint(20,70)
        elif(self.robot_x[-1]<arena_limit and self.robot_y[-1]>arena_size-arena_limit): #Left-top
            theta = np.random.randint(290,340)
        elif(self.robot_x[-1]>arena_size-arena_limit and self.robot_y[-1]<arena_limit): #Right-bottom
            theta = np.random.randint(110, 160)
        elif(self.robot_x[-1]>arena_size-arena_limit and self.robot_y[-1]>arena_size-arena_limit): #Right-top
            theta = np.random.randint(200,250)
        elif( self.robot_x[-1]<arena_limit ): #Left
            if theta <=180:
                theta -= theta_extra
            else:
                theta += theta_extra
        elif(self.robot_x[-1]>arena_size-arena_limit ): #Right
            if theta <=180 and theta >= 0:
                theta += theta_extra
            elif theta <= 0:
                theta = 0
            else:
                theta -= theta_extra
        elif(self.robot_y[-1]<arena_limit): #Bottom
            if theta >= 90 and theta <=270:
                theta -= theta_extra
            else:
                theta += theta_extra
        elif(self.robot_y[-1]>arena_size-arena_limit): #Top
            if theta >= 90 and theta <=270:
                theta += theta_extra
            else:
                theta -=theta_extra
        else:
            theta = theta + random.gauss(0, nav_noise) + self.wheel*wheel_w

        check_x = self.robot_x[-1]+self.conv(theta)[0] + np.random.uniform(-0.5,0.5)
        check_y = self.robot_y[-1]+self.conv(theta)[1] + np.random.uniform(-0.5,0.5)
        if check_x >= 4 and check_x <= arena_size - 4 and check_y >= 4 and check_y <= arena_size - 4:
            self.robot_x.append(check_x)
            self.robot_y.append(check_y)
        else:
            self.robot_x.append(self.robot_x[-1])
            self.robot_y.append(self.robot_y[-1])

    def save_data(self, current_simulation):
        global simulation, aVarousal_list, dVarousal_list, aVsecurity_list, dVsecurity_list

        csv_namefile = '/home/roboticslab/Robotology/Repos/allostractor/data/2010Replication/2gradients/' + str(current_simulation+1) + '.csv'
        print(csv_namefile)
        with open(csv_namefile, mode='w') as csv_file:
            csv_writer = csv.DictWriter(csv_file, fieldnames=['Xposition', 'Yposition', 'aVarousal', 'dVarousal', 'aVsecurity', 'dVsecurity'])
            csv_writer.writeheader()
            for i in range(episodes):
                csv_writer.writerow({'Xposition': self.robot_x[i], 'Yposition': self.robot_y[i], 'aVarousal': aVarousal_list[i], 'dVarousal': dVarousal_list[i],
                    'aVsecurity': aVsecurity_list[i], 'dVsecurity': dVsecurity_list[i]})

    def clean(self):
        global aVarousal_list, dVarousal_list, aVsecurity_list, dVsecurity_list
        self.robot_x = [random.randint(10,190)]
        self.robot_y = [random.randint(10,190)]
        security_intensity_list=[]
        arousal_intensity_list=[]
        aVarousal_list = []
        dVarousal_list = []
        aVsecurity_list = []
        dVsecurity_list = []

    def run(self, episodes, i):
        global simulation

        for j in range(episodes):
            self.arousal_av()
            self.security_av()
            self.adsign()
            self.hsign()
            self.prob_switcher()
            self.total_force()
            self.wheel_turning()
            self.random_navigation()
            if plots == True:
                self.plot_gradients()
        self.save_data(i)
        self.clean()




allo = Allostasis_2010()

if __name__ == '__main__':
    try:
        if Group_simulations == True:
            for i in range(num_simulations):
                allo.run(episodes, i)

        else:
            allo.run(episodes, 0)

    except KeyboardInterrupt:
        print('Simulation interrupted')